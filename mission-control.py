#!/usr/bin/python
# bang path allows this to be run as a script in the shell
# (assuming the Python interpreter is /usr/bin/python)

'''
mission-control

Reads a file of satellite telemetry data, determines if any satellite component
limits have been exceeded, and creates alerts based on the alert parameters
described in README.md. Prints a JSON description of the alerts to stdout.
'''

# Need these Python standard library modules
import csv
from datetime import datetime
import sys


class Error():
    '''
    A satellite error condition that could generate an alert.
    '''
    def __init__(self, desc):
        '''
        Args:
            desc: describes the error condition, like "RED LOW|BATT|3|5"

        Attributes:
            condition: string like "RED LOW" or "YELLOW HIGH"
            component: satellite component"
            count: threshold for generating an alert
            duration: if count errors seen in this many minutes, alert
        '''
        self.condition, self.component, self.count, self.duration = desc.split("|")
        self.count = int(self.count)
        self.duration = int(self.duration)

    def check(self, limits, value, component):
        '''
        Checks telemetry data against an error condition.

        Args:
            limits: list of max and min values for RED and YELLOW
            value: satellite parameter value
            component: satellite component reporting the value
        '''

        if component != self.component:
            # Satellite component doesn't match
            return False

        # Check value against limit based on level and HIGH/LOW condition
        if "HIGH" in self.condition and "RED" in self.condition:
            return value > limits[0]
        if "HIGH" in self.condition and "YELLOW" in self.condition:
            return value > limits[1]
        if "LOW" in self.condition and "YELLOW" in self.condition:
            return value < limits[2]
        if "LOW" in self.condition and "RED" in self.condition:
            return value < limits[3]


class Anomaly():
    '''
    An anomaly is more than one error. Count errors so an alert can be
    generated if more than x errors are seen in time y.
    '''
    def __init__(self, ts):
        '''
        Args:
            ts: datetime object, timestamp from telemetry data

        Attributes:
            ts: time the error was first seen
            count: how many times the alert has been seen
        '''
        self.ts = ts
        self.count = 1


class Alert():
    '''
    Create an alert when an error is seen more than x times in time y.
    '''
    def __init__(self, ts, id, error):
        '''
        Args:
            ts: datetime object, time the error was first seen
            id: integer ID of the satellite reporting the error
            error: Error that has occurred more than x times in y minutes

        Attributes:
            satelliteId: integer ID of the satellite
            severity: alert level + hi/lo ("RED HIGH", "YELLOW LOW")
            component: satellite component reporting the error
            timestamp: formatted string, time when error was first seen
        '''
        self.satelliteId = id
        self.severity = error.condition
        self.component = error.component
        self.timestamp = formatTime(ts)

    def str(self):
        '''
        Generates a JSON string for the alert in the exact format specified
        in README.md. The Python "json" module could be used instead, but
        output doesn't exactly match the desired format.

        Returns:
            string containing JSON for an alert
        '''
        s =  "    {\n"
        s += "        satelliteId: %d,\n" % self.satelliteId
        s += "        severity: \"%s\",\n" % self.severity
        s += "        component: \"%s\",\n" % self.component
        s += "        timestamp: \"%s\"\n" % self.timestamp
        s += "    }"
        return s


def formatTime(ts):
    '''
    Generate a formatted timestamp from a datetime object.

    Args:
        ts: a Python datetime object
    Returns:
        a timestamp string in the exact format described in README.md,
        like: yyyy-mm-ddThh:mm:ss.sssZ
    '''

    # datetime object stores microseconds, but we only need millisecond resolution
    # all times are Zulu (UTC)
    return ts.strftime("%Y-%d-%dT%H:%M:%S.") + "%03d" % (ts.microsecond / 1000) + "Z"


def checkTelemetry(filename):
    '''
    Read telemetry data. Print JSON output to stdout if any alert conditions
    appear in the data.

    Args:
        name of the file containing telemetry data, as described in README.md

    Data:
        errors: list of error conditions
        anomalies: dict of errors seen, with counts and start times
        alerts: list of anomalies that meet the alert criteria
    '''

    # Create the errors list. These are the specified errors, but this
    # function could also check for others like "YELLOW HIGH" etc.
    errors = []
    errors.append(Error("RED LOW|BATT|3|5"))
    errors.append(Error("RED HIGH|TSTAT|3|5"))

    # Anomaly dict will be keyed by satellite ID, error level, and component.
    anomalies = {}

    # Create the alerts listself.
    alerts = []

    # Open the telemetry file.
    # Python exception will provide error message if file can't be opened.
    infile = open(filename, "r")

    # Read the telemetry. Using the csv module is a few lines shorter than
    # splitting lines from the input file and dealing with newlines.
    data = csv.reader(infile, delimiter = '|')

    # Process each line of the telemetry data.
    for line in data:
        # Convert timestamp string to a datetime object
        timestamp = datetime.strptime(line[0], '%Y%m%d %H:%M:%S.%f')
        # Integer satellite id
        id = int(line[1])
        # Floating point high/low limits and value
        limits = [float(l) for l in line[2:6] ]
        value = float(line[6])
        component = line[7]

        # Check the telemetry line against each possible error
        for err in errors:
            if err.check(limits, value, component):
                # Current telemetry line matched an error condition.
                # Create the key value for the anomalies dict.
                key = "%d %s %s" % (id, err.condition, err.component)
                if key in anomalies:
                    # Matches an existing anomaly.
                    x = anomalies[key]
                    delta = timestamp - x.ts
                    if delta.total_seconds() <= err.duration * 60:
                        # Same error seen less than duration minutes ago?
                        x.count += 1
                        if x.count >= err.count:
                            # Reached threshold to trigger an alert.
                            alerts.append(Alert(x.ts, id, err))
                    else:
                        # Threshold not reached in time, reset this anomaly.
                        x.count = 1
                        x.ts = timestamp
                else:
                    # Does not match an existing anomaly, create one.
                    anomalies[key] = Anomaly(timestamp)

    # Print JSON representation of any alerts that were found.
    # Standard Python "json" module would produce output that is slightly
    # different from the README.md specification, so we do it this way.
    c = 0
    print "["
    for a in alerts:
        c += 1
        if c < len(alerts):
            # not the last alert, follow it with a comma
            print "%s," % a.str()
        else:
            # last alert
            print a.str()

    print "]"



# Check telemetry for a single input file when run as a script.
if __name__ == "__main__":
    if len(sys.argv) == 2:
        # Expect one command line argument for the telemetry filename.
        checkTelemetry(sys.argv[1])
    else:
        # Wrong number of arguments.
        print "Usage: mission_control.py <input file>"
